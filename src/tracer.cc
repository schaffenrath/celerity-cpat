#include "tracer.h"

#include <mutex>
#include <optional>

#include <spdlog/fmt/fmt.h>
// NOCOMMIT Move elsewhere?
#include <google/protobuf/io/zero_copy_stream_impl.h>

#include "runtime.h"      // NOCOMMIT
#include "task_manager.h" // NOCOMMIT

// NOCOMMIT MOVE ELSEWHERE. ALSO: In reserved range?!
constexpr int PORT = 4503;

namespace celerity {
namespace detail {

	// NOCOMMIT TODO We need to ensure enums stay in sync

	// NOCOMMIT Move elsewhere? Find other places this is used
	template <typename EnumT>
	constexpr auto to_underlying(EnumT value) -> std::underlying_type_t<EnumT> {
		return static_cast<std::underlying_type_t<EnumT>>(value);
	}

	// NOCOMMIT TODO Can we get any kind of safety here?
	template <typename OutT, typename InT>
	constexpr OutT enum_cast(InT value) {
		return static_cast<OutT>(to_underlying(value));
	}

	trace_file_transport::trace_file_transport(const std::filesystem::path& output_file, node_id nid, size_t num_nodes) {
		auto actual_file = output_file;
		if(num_nodes > 1) { actual_file = fmt::format("{}_{}{}", output_file.stem().string(), nid, output_file.extension().string()); }
		ofstream.open(actual_file, std::ios::binary);
		// NOCOMMIT Handle file not opened
	}

	void trace_file_transport::flush(const trace::TracePayload& payload) {
		// NOCOMMIT TODO: Probably don't recreate these streams every time
		::google::protobuf::io::OstreamOutputStream raw_output_stream(&ofstream);
		::google::protobuf::io::CodedOutputStream coded_output_stream(&raw_output_stream);
		assert(payload.ByteSizeLong() < std::numeric_limits<uint32_t>::max());
		coded_output_stream.WriteLittleEndian32(payload.ByteSizeLong());
		payload.SerializeToCodedStream(&coded_output_stream);
	}

	trace_tcp_transport::trace_tcp_transport(const std::string& ip, const std::optional<int> port) : socket(io_service) {
		boost::asio::deadline_timer deadline(io_service);
		deadline.expires_from_now(boost::posix_time::seconds(2)); // NOCOMMIT Revisit
		// tcp_socket.async_connect(tcp::endpoint("127.0.0.1", PORT), // NOCOMMIT, obviously
		// boost::bind(&profile_exporter::handle_connect, this, boost::asio::placeholders::error));
		
		boost::system::error_code error;
		// NOCOMMIT Do we need a timeout?
		// NOCOMMIT TODO: Use resolver for more flexibility (e.g. "localhost")
		socket.connect(boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(ip, error), port.has_value() ? port.value() : PORT));
		io_service.run();
	}

	trace_tcp_transport::~trace_tcp_transport() {
		if(socket.is_open()) { socket.close(); }
	}

	void trace_tcp_transport::flush(const trace::TracePayload& payload) {
		boost::asio::streambuf stream_buffer;
		std::ostream output_stream(&stream_buffer);

		{
			::google::protobuf::io::OstreamOutputStream raw_output_stream(&output_stream);
			::google::protobuf::io::CodedOutputStream coded_output_stream(&raw_output_stream);
			assert(payload.ByteSizeLong() < std::numeric_limits<uint32_t>::max());
			coded_output_stream.WriteLittleEndian32(payload.ByteSizeLong());
			payload.SerializeToCodedStream(&coded_output_stream);
		}

		boost::system::error_code error;
		socket.send(stream_buffer.data());
	}

	job_tracer::~job_tracer() {
		finish_time = tracer_clock::now();
		trc->add_job_trace(*this);
	}

	job_tracer::job_tracer(tracer* trc, command_id cid) : trc(trc), cid(cid) { start_time = tracer_clock::now(); }

	void job_tracer::add_submit_time() { job_tracer::submit_time = tracer_clock::now(); }

	void job_tracer::add_submitted_time() { job_tracer::submitted_time = tracer_clock::now(); }

	void job_tracer::add_poll_statistic(const uint64_t min_duration, const uint64_t max_duration, const uint64_t avg_duration, const uint64_t sample_count) {
		poll_min_duration = min_duration;
		poll_max_duration = max_duration;
		poll_avg_duration = avg_duration;
		poll_sample_count = sample_count;
	}


	tracer& tracer::get_instance() {
		if(instance == nullptr) { throw std::runtime_error("Tracer has not been initialized"); }
		return *instance;
	}

	void tracer::init(node_id nid, int argc, const char* const argv[]) { instance = std::unique_ptr<tracer>(new tracer(nid, argc, argv)); }

	void tracer::add_task(const task& tsk) {
		{
			std::unique_lock lock(mutex);

			// NOCOMMIT Get rid of this? The only thing we really need from here is the debug name (and even that should be replaced w/ kernel id at some
			// point).
			const auto label = ([&] {
				switch(tsk.get_type()) {
				case task_type::NOP: return fmt::format("Task {} (nop)", tsk.get_id());
				case task_type::HOST_COMPUTE: return fmt::format("Task {} (host-compute)", tsk.get_id());
				case task_type::DEVICE_COMPUTE: return fmt::format("Task {} ({})", tsk.get_id(), tsk.get_debug_name());
				case task_type::COLLECTIVE: return fmt::format("Task {} (collective #{})", tsk.get_id(), static_cast<size_t>(tsk.get_collective_group_id()));
				case task_type::MASTER_NODE: return fmt::format("Task {} (master-node)", tsk.get_id());
				default: assert(false); return fmt::format("Task {} (unknown)", tsk.get_id());
				}
			})(); // IIFE

			const auto pbt = current_payload.add_tasks();
			pbt->set_id(tsk.get_id());
			// NOCOMMIT Keep in sync!
			pbt->set_target(enum_cast<trace::ExecutionTarget>(tsk.get_execution_target()));
			pbt->set_name(label);

			for(auto d : tsk.get_dependencies()) {
				const task_id tid = d.node->get_id();
				if(tid == 0) continue; // Skip INIT task

				const auto pbd = pbt->add_dependencies();
				pbd->set_id(tid);
				pbd->set_kind(enum_cast<trace::DependencyKind>(d.kind));
			}
		}

		maybe_flush();
	}

	// The underlying integral values for SYCL access mode are not specified,
	// so we have to do a manual conversion to the Protobuf enum.
	trace::AccessMode sycl_mode_to_pb(cl::sycl::access::mode m) {
		using sm = cl::sycl::access::mode;
		using pbm = trace::AccessMode;
		switch(m) {
		case sm::read: return pbm::READ;
		case sm::write: return pbm::WRITE;
		case sm::read_write: return pbm::READ_WRITE;
		case sm::discard_write: return pbm::DISCARD_WRITE;
		case sm::discard_read_write: return pbm::DISCARD_READ_WRITE;
		case sm::atomic: return pbm::ATOMIC;
		default: throw std::runtime_error("Unexpected mode");
		}
	}

	void tracer::add_command_buffer_access(command_id cid, buffer_id bid, GridRegion<3> region, cl::sycl::access::mode mode) {
		std::unique_lock lock(mutex);

		// NOCOMMIT Same as below - move elsewhere
		const auto box_to_pb = [](const GridBox<3>& box, trace::Box3D& pb_box) {
			pb_box.set_min0(box.get_min().x);
			pb_box.set_min1(box.get_min().y);
			pb_box.set_min2(box.get_min().z);
			pb_box.set_max0(box.get_max().x);
			pb_box.set_max1(box.get_max().y);
			pb_box.set_max2(box.get_max().z);
		};

		auto& accesses = command_buffer_accesses[cid];
		region.scanByBoxes([&](const GridBox<3>& box) {
			auto ba = std::make_unique<trace::BufferAccess>();
			ba->set_buffer_id(bid);
			box_to_pb(box, *ba->mutable_range());
			ba->set_mode(sycl_mode_to_pb(mode));
			accesses.emplace_back(std::move(ba));
		});
	}

	void tracer::add_command(const abstract_command& cmd, const std::vector<command_id>& dependencies /* NOCOMMIT currently unused - we do need kind... */) {
		{
			std::unique_lock lock(mutex);

			// NOCOMMIT Same as below - move elsewhere
			const auto box_to_pb = [](const GridBox<3>& box, trace::Box3D& pb_box) {
				pb_box.set_min0(box.get_min().x);
				pb_box.set_min1(box.get_min().y);
				pb_box.set_min2(box.get_min().z);
				pb_box.set_max0(box.get_max().x);
				pb_box.set_max1(box.get_max().y);
				pb_box.set_max2(box.get_max().z);
			};

			command_type type = command_type::NOP;
			std::optional<task_id> tid = std::nullopt;
			std::optional<GridBox<3>> execution_range = std::nullopt;
			std::optional<node_id> other_nid = std::nullopt;
			std::optional<buffer_id> bid = std::nullopt;

			if(isa<task_command>(&cmd)) {
				tid = static_cast<const task_command&>(cmd).get_tid();
				type = command_type::TASK;
				execution_range = subrange_to_grid_box(static_cast<const task_command&>(cmd).get_execution_range());
			} else if(isa<push_command>(&cmd)) {
				type = command_type::PUSH;
				other_nid = static_cast<const push_command&>(cmd).get_target();
				bid = static_cast<const push_command&>(cmd).get_bid();
			} else if(isa<await_push_command>(&cmd)) {
				type = command_type::AWAIT_PUSH;
				other_nid = static_cast<const await_push_command&>(cmd).get_source()->get_nid();
				bid = static_cast<const await_push_command&>(cmd).get_source()->get_bid();
			} else if(isa<horizon_command>(&cmd)) {
				type = command_type::HORIZON;
			} else {
				throw std::runtime_error("Unexpected command type");
			}

			const command_id cid = cmd.get_cid();

			const auto pbc = current_payload.add_commands();
			pbc->set_id(cid);
			pbc->set_node_id(cmd.get_nid());
			pbc->set_type(enum_cast<trace::CommandType>(type));
			if(tid.has_value()) { pbc->set_task_id(*tid); }
			if(other_nid.has_value()) { pbc->set_other_node_id(*other_nid); }
			if(bid.has_value()) { pbc->set_buffer_id(*bid); }
			if(execution_range.has_value()) { box_to_pb(*execution_range, *pbc->mutable_execution_range()); }
			for(const auto& ba : command_buffer_accesses[cid]) {
				// NOCOMMIT Don't copy here, set arena allocated array directly.
				// See https://stackoverflow.com/questions/57458319/c-protobuf-add-an-already-allocated-repeated-numeric-field
				const auto a = pbc->add_buffer_accesses();
				*a = *ba;
			}
			command_buffer_accesses.erase(cid);

			for(const auto& d : cmd.get_dependencies()) {
				const auto pbd = pbc->add_dependencies();
				pbd->set_id(d.node->get_cid());
				pbd->set_kind(enum_cast<trace::DependencyKind>(d.kind));
			}
		}

		maybe_flush();
	}

	std::unique_ptr<job_tracer> tracer::trace_job(command_id cid) {
		// TODO: Consider pooling these
		return std::unique_ptr<job_tracer>(new job_tracer(this, cid));
	}

	void tracer::shutdown() { flush(true); }

	tracer::tracer(node_id this_nid, int argc, const char* const argv[]) {
		GOOGLE_PROTOBUF_VERIFY_VERSION;

		current_payload.set_source_node_id(this_nid);

		auto run_info = current_payload.mutable_run_info();
		// NOTE: Until C++20 system_clock's epoch is not guaranteed to be the UNIX epoch (we just assume that it is).
		const auto unix_time_now = std::chrono::system_clock::now().time_since_epoch();
		run_info->set_reference_time_point(std::chrono::duration_cast<std::chrono::microseconds>(unix_time_now).count());
		if(argc > 0) { run_info->set_executable_name(argv[0]); }
		for(int i = 1; i < argc; ++i) {
			run_info->add_executable_args(argv[i]);
		}
	}

	void tracer::add_job_trace(const job_tracer& jt) {
		{
			std::unique_lock lock(mutex);

			const auto pbj = current_payload.add_jobs();
			pbj->set_command_id(jt.cid);
			pbj->set_start_time(get_relative_time(jt.start_time));
			pbj->set_finish_time(get_relative_time(jt.finish_time));
			pbj->set_submit_time(get_relative_time(jt.submit_time));
			pbj->set_submitted_time(get_relative_time(jt.submitted_time));
			auto pb_poll_statistic = pbj->mutable_poll_statistic();
			pb_poll_statistic->set_min_duration(jt.poll_min_duration);
			pb_poll_statistic->set_max_duration(jt.poll_max_duration);
			pb_poll_statistic->set_avg_duration(jt.poll_avg_duration);
			pb_poll_statistic->set_sample_count(jt.poll_sample_count);
		}
		maybe_flush();
	}

	void tracer::maybe_flush() {
		bool do_flush = false;
		{
			// std::shared_lock lock(mutex); // NOCOMMIT 1) This shouldnt be needed as ByteSizeLong is const. 2) We need a unique lock for flushing
			std::unique_lock lock(mutex); // We get crashes without this - WHY?!

			// NOCOMMIT Figure out good threshold - what do we want to optimize here..?
			constexpr size_t ARBITRARY_THRESHOLD = 32 * 1024;
			// NOCOMMIT How expensive is this? Seems expensive!
			if(current_payload.ByteSizeLong() > ARBITRARY_THRESHOLD) {
				// flush(false);
				do_flush = true;
			}
		}
		if(do_flush) { flush(false); }
	}

	void tracer::flush(bool is_final_payload) {
		std::unique_lock lock(mutex);
		if(is_final_payload) { current_payload.set_is_final_payload(true); }

		assert(!transports.empty());
		for(auto& t : transports) {
			t->flush(current_payload);
		}

		if(!is_final_payload) {
			current_payload.clear_commands();
			current_payload.clear_jobs();
			current_payload.clear_run_info();
			current_payload.clear_tasks();
		}
	}

} // namespace detail
} // namespace celerity
