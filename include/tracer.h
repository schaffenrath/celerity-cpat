#pragma once

#include <chrono>
#include <filesystem>
#include <fstream>
#include <memory>
#include <optional>
#include <shared_mutex>

#include <boost/asio.hpp> // NOCOMMIT Move elsewhere?

#include "grid.h"
#include "task.h"
#include "trace.pb.h"
#include "types.h"

// NOCOMMIT Temporarily used for command_type, remove
#include "command.h"

namespace celerity {
namespace detail {
	class abstract_command;

	class trace_transport {
	  public:
		virtual ~trace_transport() = default;
		virtual void flush(const trace::TracePayload& payload) = 0;
	};

	class trace_file_transport : public trace_transport {
	  public:
		trace_file_transport(const std::filesystem::path& output_file, node_id nid, size_t num_nodes);
		void flush(const trace::TracePayload& payload) override;

	  private:
		std::ofstream ofstream;
	};

	class trace_tcp_transport : public trace_transport {
	  public:
		explicit trace_tcp_transport(const std::string& ip, const std::optional<int> port);
		~trace_tcp_transport();

		void flush(const trace::TracePayload& payload) override;

	  private:
		boost::asio::io_service io_service;
		boost::asio::ip::tcp::socket socket;
	};

	using tracer_clock = std::chrono::steady_clock;
	using tracer_time_point = tracer_clock::time_point;

	class tracer;

	class job_tracer {
		friend class tracer;

	  public:
		~job_tracer();

		/**
		 * Adds the time when either submit is called for a device job to the queue, a host task is scheduled in the thread pool or
		 * submit is called for a push job to the BTM.
		 *
		 * => TODO Find more general name
		 */
		void add_submit_time();

		/**
		 * Adds the time when the submission is done for the cases mentioned in the add_submit_time() function.
		 */
		void add_submitted_time();

		/**
		 * Adds the poll statistic of a job. The job itself must not have been added yet.
		 */
		void add_poll_statistic(const uint64_t min_duration, const uint64_t max_duration, const uint64_t avg_duration, const uint64_t sample_count);


	  private:
		job_tracer(tracer* trc, command_id cid);
		tracer* trc;
		command_id cid;
		tracer_time_point start_time;
		tracer_time_point finish_time;
		tracer_time_point submit_time;
		tracer_time_point submitted_time;
		uint64_t poll_min_duration;
		uint64_t poll_max_duration;
		uint64_t poll_avg_duration;
		uint64_t poll_sample_count;

	};

	// NOCOMMIT TODO: So we need to figure out a good way of measuring times and getting them into relation.
	// ==> For accurate timings, we have to use chrono::steady_clock (high_resolution_clock MIGHT be an alias for system_clock!)
	// BUT: There is no way of relating steady_clock time points to an absolute reference (e.g. UNIX epoch).
	// While this means that we can log traces relative to some arbitrary start value (possibly system up time, or anything really)
	// we cannot use that to synchronize traces between different nodes. For that we'd still need to log a system_clock time stamp
	// (and hope that the different cluster nodes are accurately synced up).
	// => TODO Also look into MPI_Wtime, and consider doing a manual (and crude) clock sync using a barrier
	// => The trace should contain meta info that e.g. warns if clocks are potentially not 100% synced!
	// NOCOMMIT TODO: Think about thread safety
	class tracer {
		friend class job_tracer;

	  public:
		// NOCOMMIT TODO: Do we want this to be a singleton? What about testability?
		static tracer& get_instance();

		/**
		 * Initializes this tracer. This must be the first interaction with the tracer.
		 *
		 * @param nid The id of the node this tracer is being initialized in.
		 * @param argc The number of arguments provided to the executable.
		 * @param argv The arguments provided to the executable.
		 */
		static void init(node_id nid, int argc, const char* const argv[]);

		/**
		 * Adds a trace transport to the list of transports the tracer will flush to.
		 *
		 * Note that adding transports must be done as early as possible, before
		 * any data is flushed. Otherwise newly added transports will receive incomplete data.
		 */
		void add_transport(std::unique_ptr<trace_transport>&& transport) { transports.emplace_back(std::move(transport)); }

		/**
		 * Adds a task to the trace data.
		 */
		void add_task(const task& tsk);

		/**
		 * Adds a buffer access for a command. The command itself must not have been added yet.
		 *
		 * @param cid The command id for which to add the buffer access.
		 * @param bid The buffer that is being accessed by the command.
		 * @param region The region of the buffer being accessed.
		 * @param mode The mode used for the access.
		 */
		void add_command_buffer_access(command_id cid, buffer_id bid, GridRegion<3> region, cl::sycl::access::mode mode);

		/**
		 * Adds a command to the trace data. All buffer accesses must have been added at this point.
		 *
		 * @see tracer::add_command_buffer_access
		 *
		 * @param cmd The command to be added.
		 * @param dependencies The list of commands @p cmd depends on (this is purely an optimization, as we could compute this from @p cmd itself).
		 */
		void add_command(const abstract_command& cmd, const std::vector<command_id>& dependencies);

		std::unique_ptr<job_tracer> trace_job(command_id cid);

		/**
		 * @brief Flushes all remaining trace data.
		 *
		 * NOCOMMIT Make static and reset instance?
		 */
		void shutdown();

	  private:
		inline static std::unique_ptr<tracer> instance = nullptr;

		// All time points within the generated trace are relative to this (arbitrary!) time point.
		tracer_time_point reference_time_point;

		mutable std::shared_mutex mutex; // NOCOMMIT Do we even have any shared accesses? Looks like no
		// NOCOMMIT TODO Periodically flush this
		trace::TracePayload current_payload;
		std::vector<std::unique_ptr<trace_transport>> transports;

		std::unordered_map<command_id, std::vector<std::unique_ptr<trace::BufferAccess>>> command_buffer_accesses;

		tracer(node_id nid, int argc, const char* const argv[]);

		void add_job_trace(const job_tracer& jt);

		/**
		 * Returns a time point relative to the reference time point, in microseconds.
		 */
		uint64_t get_relative_time(tracer_time_point tp) const {
			return std::chrono::duration_cast<std::chrono::microseconds>(tp - reference_time_point).count();
		}

		void maybe_flush();

		void flush(bool is_final_payload);
	};

} // namespace detail
} // namespace celerity
